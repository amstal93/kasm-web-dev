login:
	docker login 
	docker login registry.gitlab.com

build:
	DOCKER_BUILDKIT=1 docker build -f Dockerfile -t oytunistrator/kasm-web-dev:dev .

push:
	docker push oytunistrator/kasm-web-dev:dev

release:
	DOCKER_BUILDKIT=1 docker build -f Dockerfile -t oytunistrator/kasm-web-dev:1.0 .

push_rel:
	docker push oytunistrator/kasm-web-dev:1.0


build_gitlab:
	DOCKER_BUILDKIT=1 docker build -t registry.gitlab.com/oytunistrator/kasm-web-dev .

push_gitlab:
	docker push registry.gitlab.com/oytunistrator/kasm-web-dev

test:
	docker run --rm -i -t oytunistrator/kasm-web-dev:dev



build_all: build release build_gitlab
push_all: push push_rel push_gitlab

auto: build_all push_all

all: build push

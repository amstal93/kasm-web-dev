#!/bin/bash
set -ex

add-apt-repository ppa:openshot.developers/ppa
add-apt-repository -y ppa:obsproject/obs-studio

apt-get update
apt-get install -y mesa-utils libglu1-mesa-dev freeglut3-dev mesa-common-dev vlc obs-studio openshot-qt python3-openshot

wget -q https://github.com/CatxFish/obs-v4l2sink/releases/download/0.1.0/obs-v4l2sink.deb
apt-get install ./obs-v4l2sink.deb
rm -f obs-v4l2sink.deb

#!/bin/bash
set -ex

apt-get update 
apt-get upgrade -y
apt-get install -y lsb-release ca-certificates apt-transport-https software-properties-common filezilla nano vim build-essential golang mousepad links lynx ruby virt-manager git remmina rdesktop python3 python3-pip python3 python python-pip python-dev python3-venv build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget
